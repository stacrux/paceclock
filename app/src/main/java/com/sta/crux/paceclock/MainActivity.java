package com.sta.crux.paceclock;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences preferences;
    private Map<String, Drawable> watchFacesResourceMap;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        watchFacesResourceMap = generateResourceMap();

        setupMainView();
        setupClockFace();
        setupClockHandles();
    }

    private void setupMainView() {
        View mainView = findViewById(R.id.main_view);
        int backgroundColor = preferences.getInt("backgroundColor",
                getResources().getColor(R.color.colorPrimary));
        mainView.setBackgroundColor(backgroundColor);
        mainView.invalidate();
    }

    private Map<String, Drawable> generateResourceMap() {
        Map<String, Drawable> resMap = new HashMap<>();
        Field[] fields = R.drawable.class.getFields();
        Context ctx = this;
        for (int  i = 0; i < fields.length; i++) {
            String name = fields[i].getName();
            if (!name.contains("clock")){
                continue;
            }
            int resID = ctx.getResources().getIdentifier(name, "drawable", ctx.getPackageName());
            Drawable value = ctx.getResources().getDrawable( resID );
            resMap.put(name, value);
        }
        return resMap;
    }

    private void setupClockHandles() {

        ImageView clockHands = findViewById (R.id.lancette);
        String selectedClockHandles = preferences.getString("clockHandles", "thinHandles");
        if (selectedClockHandles.equals("thinHandles")){
            clockHands.setImageResource(R.drawable.cross);
        } else {
            clockHands.setImageResource(R.drawable.cross_big);
        }
        final TextView counter = findViewById(R.id.counter);

        RotationHelper.setupQuarterRotations(clockHands, this);

        // setting the click behaviour
        clockHands.setOnLongClickListener(
                new View.OnLongClickListener()
                {
                    @Override
                    public boolean onLongClick(View v) {
                        counter.setText("-1");
                        return false;
                    }
                }
        );
        clockHands.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View v){
                        counter.setText( Integer.toString(Integer.parseInt(counter.getText().toString()) + 1)   );
                    }
                }
        );
    }

    private void setupClockFace() {
        ImageView clock = findViewById(R.id.clock);
        String selectedClockFace = preferences.getString("clockFace", "modern_clock_template");
        clock.setImageDrawable(watchFacesResourceMap.get(selectedClockFace));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.help) {
            openDialog();
            return true;
        }
        if(id == R.id.theme){
            View mainView = findViewById(R.id.main_view);
            Random rnd = new Random(new Random().nextLong());
            // pick a color on the blue tint
            int randomBackgroundColor = Color.argb(255,
                    rnd.nextInt(30),
                    rnd.nextInt(30),
                    rnd.nextInt(160));
            mainView.setBackgroundColor(randomBackgroundColor);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("backgroundColor", randomBackgroundColor);
            editor.apply();
            mainView.invalidate();
            return true;
        }
        if(id == R.id.change_clock) {
            List<String> watchFaces = new ArrayList<>(watchFacesResourceMap.keySet());
            ImageView clock = findViewById(R.id.clock);
            String selectedClockFace = preferences.getString("clockFace", "modern_clock_template");
            Drawable nextWatchFace = getNextWatchFace(watchFaces, watchFaces.indexOf(selectedClockFace));
            clock.setImageDrawable(nextWatchFace);
            return true;
        }
        if(id == R.id.change_tickers) {
            ImageView clockHands = findViewById(R.id.lancette);
            String selectedClockHandles = preferences.getString("clockHandles", "thinHandles");
            if (selectedClockHandles.equals("thinHandles")) {
                clockHands.setImageResource(R.drawable.cross_big);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("clockHandles", "thickHandles");
                editor.apply();
            } else {
                clockHands.setImageResource(R.drawable.cross);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("clockHandles", "thinHandles");
                editor.apply();
            }
            return true;
        }
        if (id == R.id.quit){
            this.finish();
        }
        if (id == R.id.toggle_sound){
            if (preferences.getBoolean("soundEnabled", false)){
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("soundEnabled", false);
                editor.apply();
                Toast.makeText(this, "Sound Disabled",
                        Toast.LENGTH_LONG).show();
            } else {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("soundEnabled", true);
                editor.apply();
                Toast.makeText(this, "Sound Enabled",
                        Toast.LENGTH_LONG).show();
            }
            RotationHelper.setupQuarterRotations(findViewById (R.id.lancette), this);
        }
        if (id == R.id.change_sound_set){
            if (preferences.getString("soundSet", "Notes").equals("Notes")){
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("soundSet", "Dings");
                editor.apply();
                RotationHelper.setupQuarterRotations(findViewById (R.id.lancette), this);
                Toast.makeText(this, "Special Sounds Selected",
                        Toast.LENGTH_LONG).show();
            } else {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("soundSet", "Notes");
                editor.apply();
                Toast.makeText(this, "Notes Selected",
                        Toast.LENGTH_LONG).show();
            }
            RotationHelper.setupQuarterRotations(findViewById (R.id.lancette), this);

        }
        if (id == R.id.reset){
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("clockHandles", "thinHandles");
            editor.putString("clockFace", "modern_clock_template");
            editor.putInt("backgroundColor",
                    getResources().getColor(R.color.colorPrimary));
            editor.apply();
            setupMainView();
            setupClockFace();
            setupClockHandles();
        }
        return super.onOptionsItemSelected(item);
    }

    private Drawable getNextWatchFace(List<String> watchFaces, int currentFaceIndex) {

        Drawable nextFace;
        String nextFaceName;
        if (currentFaceIndex + 1 == watchFaces.size()) {
            nextFaceName = watchFaces.get(0);
        } else {
            nextFaceName = watchFaces.get(currentFaceIndex + 1);
        }
        nextFace = watchFacesResourceMap.get(nextFaceName);
        Toast.makeText(this, nextFaceName.split("_")[0],
                Toast.LENGTH_SHORT).show();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("clockFace", nextFaceName);
        editor.apply();
        return nextFace;
    }

    public void openDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_help);
        dialog.setTitle("Help");
        dialog.show();
        Button dismiss_dialog = (Button) dialog.findViewById(R.id.dialog_ok);
        dismiss_dialog.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                }
        );
    }

}