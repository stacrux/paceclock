#PaceClock

Perform your exercises quicker with this hands-free pace timer. 
You can touch the clock to increment the sets count, long press the clock to reset the counter to zero.
The app is meant to be basic.


check it out in the [Google Play Store](https://play.google.com/store/apps/details?id=com.sta.crux.paceclock)